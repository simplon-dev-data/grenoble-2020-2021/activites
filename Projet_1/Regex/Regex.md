# Activité sur les expressions régulières

## Recherche d'informations

Avant de voir le fonctionnement des expressions régulières, voici quelques questions :

- A quoi servent les expressions regulières ?

- Donner des exemples de cas d'usage d'expressions régulières

Toute la suite de l'activité va être effectuée sur le site https://regex101.com/ qui permet de voir de fonctionnement des expressions régulières que l'on crée. Par la suite, on verra l'utilisation d'expressions régulières en lignes de commandes et dans du code. 

## Les caractères normaux

Un usage basique des expressions régulières est la recherche d'un caractère ou d'une suite de caractères dans un texte. Pour cela, on tape le caractère ou la suite de caractères que l'on cherche à détecter comme on le fait avec la fonction "Rechercher" d'un éditeur de texte.

![Caractères normaux](caracteres_normaux.png)

On peut aussi chercher un caractère parmi un ensemble de caractères. Pour cela, on utilise la syntaxe suivante : `[abc]` pour détecter un `a`, un `b` ou un `c`.

![Caractères normaux](caracteres_normaux_2.png)

Si on veut trouver plusieurs mots différents, on peut utiliser un "OU" logique `|`. Par exemple, pour détecter "Corbeau" et "Renard", on utilise la syntaxe `Corbeau|Renard`

![Caractères normaux](caracteres_normaux_3.png)

### Mise en application

A vous de mettre en pratique cette notion :

- Sur le site https://regex101.com/ copier le texte suivant dans la partie "TEST STRING" :
```
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Inscrire un titre ici</title>
    <!-- On peut avoir d'autres méta-données ici -->
  </head>
  <body>
    <!-- Ici, on placera tout le contenu à destination 
    de l'utilisateur -->
  </body>
</html>
```

- Détecter les mots `head`, `body`, et `html` à l'aide d'une expression régulière à ajouter dans la partie "REGULAR EXPRESSION". Le résultat doit correspondre à cela :

![Caractères normaux](caracteres_normaux_4.png)

## Les caractères spéciaux (ou metacaractères)

Certains caractères ont une signification particulière dans les expressions régulières. Voici l'ensemble des caratères spéciaux qu'il faut échapper pour les utiliser dans leur sens littéral : `^ $ \ | { } [ ] ( ) ? # ! + * .`

### Exemple d'utilisation

On peut chercher à détecter le motif `e.` pour détecter les phrases dont le dernier mot se termine par `e`. Pour cela, on peut utiliser l'expression régulière suivante :

![Caractères spéciaux](caracteres_speciaux.png)

Si on supprime le caractère d'échappement `\`, le caractère `.` redevient **spécial** et reprend son rôle de caractère de remplacement pour **n'importe quel caractère**.

![Caractères spéciaux](caracteres_speciaux_2.png)

### Mise en application

A vous de mettre en pratique cette notion :

- Sur le site https://regex101.com/ copier le texte suivant dans la partie "TEST STRING" :
```
- Les grèves ? Des bêtises !
Puis, au milieu du silence fâché qui s’était fait, il ajouta doucement :
- En somme, je ne dis pas non, si ça vous amuse : ça ruine les uns, ça tue les autres, et c’est toujours autant de nettoyé... Seulement, de ce train-là, on mettrait bien mille ans pour renouveler le monde. Commencez donc par me faire sauter ce bagne où vous crevez tous !
Germinal, Emile Zola
```

- Détecter tous les caractères de ponctuation `.,?!:-` dans le texte à l'aide d'une expression régulière à ajouter dans la partie "REGULAR EXPRESSION". Le résultat doit correspondre à cela :

![Exercice Caractères spéciaux](caracteres_speciaux_3.png)

## Les quantificateurs

Il est possible de détecter la répétition de caractères (ou d'une suite de caractères) grâce à des quantificateurs.
Les quantificateurs principaux sont :

- `?` : le groupe est présent zéro ou une fois
- `*` : le groupe est présent zéro ou plusieurs fois
- `+` : le groupe est présent une ou plusieurs fois

Il existe aussi des quantificateurs pour détecter un nombre précis d'apparitions d'un motif :

- `{n}` : le groupe est présent exactement n fois 
- `{n,m}` : le groupe est présent entre n et m fois
- `{,m}` : le groupe est présent entre zéro et m fois
- `{n,}` : le groupe est présent entre n et plus de n fois

Donc, on a les équivalences suivantes :

- `?` est équivalent à `{0,1}`
- `+` est équivalent à `{1,}`
- `*` est équivalent à `{0,}`

### Exemple d'utilisation

On peut détecter tous les nombres dans un texte à l'aide de l'expression régulière suivante :

![Quantificateur](quantificateurs_2.png)

### Mise en application

A vous de mettre en pratique cette notion :

- Sur le site https://regex101.com/ copier le texte suivant dans la partie "TEST STRING" :
```
VALIDE
121112-01010-4545-67676
11221-0110-455-6677
121112-01010-4545-77
121112-01010-4545-6
NON VALIDE
1212-0100-445-
121112-01010-4545-898
1211-01010-4545-676
```

- En utilisant des quantificateurs, trouver une expression régulière permettant de détecter tous les numéros indiqués comme "VALIDE" et ne détectant aucuns des numéros indiqués comme "NON VALIDE". Le résultat doit correspondre à cela :

![Exercice quantificateur](quantificateurs.png)

## Les ensembles de caractères

Les ensemble de caractères normaux ont été vus dans la première partie de cette activité. Nous allons voir les utilisations plus poussées des ensembles.

Il existe des raccourcis pour les ensembles longs. On utilise la syntaxe suivante `[début de l'intervalle-fin de l'intervalle]`. Voici quelques exemples :
- `[a-z]` est équivalent à `[abcdefghijklmonpqrstuvwxyz]`
- `[A-Z]` est équivalent à `[ABCDEFGHIJKLMNOPQRSTUVWXYZ]`
- `[0-9]` est équivalent à `[0123456789]`

Il existe aussi des classes abrégées pour désigner des ensembles :
- `\d` est équivalent à `[0-9]`
- `\w` est équivalent à `[a-zA-Z0-9_]`

### Mise en application

A vous de mettre en pratique cette notion :

- Sur le site https://regex101.com/ copier le texte suivant dans la partie "TEST STRING" :

```
54a51c554d5212e121
45455e21212f21255g22248
86454351e35355a54564b65
545464654ef65456s546532
5455655b53c115dc545654
2135486a64b4654654c546
```

- En utilisant les raccourcis sur les ensembles de caractères, trouver une expression régulière permettant de détecter uniquement les nombres hexadécimaux (les lettres a à f sont en minuscules). Le résultat doit correspondre à cela :

![Exercice ensembles](ensembles.png)

## Les groupes

Les groupes permettent de définir des sous-motif dans l'expression régulière à l'aide de parenthèses `()`. 

### Exemple d'utilisation

On peut par exemple récupérer les balises HTML qui contiennent exactement 5 caractères entre les `<>` :

![Groupes](groupes.png)

### Mise en application

A vous de mettre en pratique cette notion :

- Sur le site https://regex101.com/ copier le texte suivant dans la partie "TEST STRING" :

```
sopwith@comcast.net
grady@aol.com
evilopie@yahoo.ca
subir@sbcglobal.net
daveed@me.com
fairbank@mac.com
blixem@att.net
enintend@live.com
aibrahim@icloud.com
phyruxus@att.net
podmaster@optonline.net
rwelty@mac.com
```

- En utilisant des groupes, récupérer les 3 parties de l'adresse email. Le résultat doit correspondre à cela (groupe 1 en vert, groupe 2 en rouge et groupe 3 en orange) :

![Exercice Groupes](groupes_2.png)

## Les extrémités du texte

Il existe des caractères spéciaux pour spécifier que le motif à rechercher doit se trouver au début ou à la fin du texte (ou de la ligne suivant les paramètres de la regex). Le caractère `^` spécifie que le motif doit se situer au début du texte. Le caractère `$` spécifie que le motif doit se situer à la fin du texte. 

### Exemple d'utilisation

Pour vérifier que chaque ligne commence par une majuscule et finit par un point, on peut utiliser l'expression régulière suivante :

![Extremites](extremites_2.png)

### Mise en application

A vous de mettre en pratique cette notion :

- Sur le site https://regex101.com/ copier le texte suivant dans la partie "TEST STRING" :

```
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Inscrire un titre ici</title>
    <!-- On peut avoir d'autres méta-données ici -->
  </head>
  <body>
    <!-- Ici, on placera tout le contenu à destination 
    de l'utilisateur -->
  </body>
</html>
```

- En utilisant les caractères spéciaux `^` et `$`, détecter les lignes de comme HTML qui commencent par `<` et finissent par `>`. Le résultat doit correspondre à cela :

![Exercice extremites](extremites.png)