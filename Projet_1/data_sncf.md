# Activité sur les données de la SNCF

![Slogan SNCF](https://www.strategies.fr/sites/default/files/styles/article_main_w600/public/assets/images/strats-article-3328-web-4.jpeg?itok=0iRDGOvl)

## Présentation de l'activté

Pour cette activité, vous allez travailler sur des données de la SNCF ( [Open Data SNCF - Régularité mensuelle TGV](https://ressources.data.sncf.com/explore/dataset/regularite-mensuelle-tgv-aqst/information/) ). Le but de l'activité est d'étudier les **retards des TGV** en **gare de Grenoble**. Le jeu de données sur lequel vous allez travailler contient les retards des TGV sur l'**ensemble de la France** entre **janvier 2015** et **juin 2020**.

Dans un **premier temps**, vous allez sélectionner uniquement les données concernant la **gare de Grenoble**. Dans un **second temps**, vous allez **analyser** cette extraction de la base de données initiale pour **répondre à des questions**. 

Toutes les actions à effectuer sur le jeu de données ont **déjà été vues** durant la **prairie**.


## Etapes de l'activité

- [ ] Créer une branche à votre nom et copier dans ce fichier les commandes exécutées dans le terminal (ça vous sera très utile pour créer le script à la fin de l'activité)

- [ ] Télécharger en ligne de commandes le jeu de données à partir de l'URL suivante : https://ressources.data.sncf.com/explore/dataset/regularite-mensuelle-tgv-aqst/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=%3B

Le fichier doit être nommé "data_SNCF.csv"

- [ ] Afficher les 10 premières lignes du fichier "data_SNCF.csv" grâce à la commande suivante :
```bash
head data_SNCF.csv
```

- [ ] Créer une base de données SQLite nommée "database_SNCF.db"

- [ ] Créer une table dans cette base de données à partir du fichier "data_SNCF.csv"

- [ ] Vérifier le contenu de la table créée

- [ ] Créer une nouvelle table avec uniquement les données concernant la gare de Grenoble (en tant que gare de départ ou d'arrivée)

- [ ] Répondre aux questions suivantes (utiliser d'abord SQLite pour répondre à ces questions puis vérifier les résultats avec un tableur):

Combien y a-t-il de TGV au départ de Grenoble dans la base de données ?

Combien y a-t-il de TGV dont l'arrivée est à Grenoble dans la base de données ?

**Questions plus compliquées :**

Quel est le pourcentage moyen de TGV en retard à l'arrivée (le pourcentage est à calculer par rapport au nombre de TGV réellement en circulation) pour les TGV au départ de Grenoble ?

Quel est le pourcentage moyen de TGV en retard à l'arrivée (le pourcentage est à calculer par rapport au nombre de TGV réellement en circulation) pour les TGV à destination de Grenoble ?

En moyenne, le retard à l'arrivée du TGV est-il plus important si je pars de la gare de Grenoble ou si j'arrive à la gare de Grenoble (en ne prenant en compte que les TGV en retard) ?

- [ ] Extraire la table des données de la gare de Grenoble au format CSV

- [ ] A l'aide d'un tableur, visualiser les retards moyens à l'arrivée et au départ de la gare de Grenoble en fonction des mois de l'année grâce à un graphique

- [ ] Créer un script pour automatiser toutes les taches depuis le téléchargement du fichier CSV jusqu'à la création des fichier CSV d'analyse depuis la base de données SQLite

